FROM ubuntu:17.10

MAINTAINER nilit


RUN apt update

###############################################################################
# frontend
###############################################################################
RUN apt --yes install nodejs

# yarn
RUN apt --yes install curl
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
RUN apt update && apt --yes install yarn

# for selenium 
RUN apt --yes install default-jre

# chrome
RUN apt --yes install wget
RUN wget --quiet --output-document - \
    https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
RUN sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" \
    >> /etc/apt/sources.list.d/google.list'
RUN apt update
RUN apt --yes install google-chrome-stable

# firefox & chromium
RUN apt --yes install firefox chromium-browser

# for the display of browsers
RUN apt --yes install xvfb

###############################################################################
# backend
###############################################################################

RUN apt --yes install python3.5 python3-dev python3-pip git

# for publishing to PyPi
RUN apt --yes install twine python3-setuptools

# for PIL
RUN apt --yes install libjpeg-dev zlib1g-dev
# for cryptography
RUN apt --yes install libffi-dev libssl-dev
# for postgres (psycopg2)
RUN apt --yes install libpq-dev

# shuup
RUN apt --yes install npm
RUN pip3 install wheel
RUN git clone https://gitlab.com/nilit/shuup-py3.git /root/shuup-py3
RUN pip3 install -e /root/shuup-py3/[everything]
RUN echo '{ "allow_root": true }' | tee /root/.bowerrc
RUN python3 /root/shuup-py3/setup.py build_resources
